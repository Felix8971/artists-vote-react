

AccountsUIWrapper = React.createClass({
  componentDidMount() {
    // Use Meteor Blaze to render login buttons
    this.view = Blaze.render(Template.loginButtons,
      ReactDOM.findDOMNode(this.refs.container));
  },

  componentWillUnmount() {
    // Clean up Blaze view
    Blaze.remove(this.view);
  },

  
  //oldWay: function (a, b) { ... },
  //newWay(a, b) { ... }

  render() { //Equivalent à render: function () { ... 
    // Just render a placeholder container that will be filled in
    return <span ref="container" />;
  }

});

