// Task component - represents a single artist 
Task = React.createClass({
  propTypes: {
    // This component gets the artist to display through a React prop.
    // We can use propTypes to indicate it is required

    artist: React.PropTypes.object.isRequired

  },


  voteForThisArtist() {
    if ( Meteor.user() ){
      var majTopArtist = function(){ //mise à jour du champ top-artist
        var topArtist = Artistes.find({}, {sort: {vote: -1}, limit: 1}).fetch();
        //console.log('top:'+topArtist[0].name);
        $('#top-artist').html(' ' + topArtist[0].name+ ' !'); 
      };

      Meteor.call("voter",this.props.artist._id, 1);
      majTopArtist();
      //setTimeout(function(){ var x = 1; },1000);
    }else{
      alert('Vous devez etre connecté pour voter !');
    } 
   
    // if ( Meteor.user() ){
    //   Artistes.update(this.props.artist._id, {
    //     $set: {vote: this.props.artist.vote+1}
    //   });

    //   //mise à jour du champ top-artist
    //   majTopArtist();
    // }else{
    //   alert('Vous devez etre connecté pour voter !');
    // }
  },


  unVoteForThisArtist() {

    if ( Meteor.user() ){
      var majTopArtist = function(){ //mise à jour du champ top-artist
        var topArtist = Artistes.find({}, {sort: {vote: -1}, limit: 1}).fetch();
        //console.log('top:'+topArtist[0].name);
        $('#top-artist').html(' ' + topArtist[0].name+ ' !'); 
      };

      Meteor.call("voter",this.props.artist._id, -1);
      majTopArtist();

    }else{
      alert('Vous devez etre connecté pour voter !');
    }    

    // if ( Meteor.user() ){
    //   Artistes.update(this.props.artist._id, {
    //     $set: {vote: this.props.artist.vote-1}
    //   });

    //   majTopArtist();
    // }else{
    //   alert('Vous devez etre connecté pour voter !');
    // }    
  },


   //pour le style dans les balises on utilise className au lieu de class
   //pour utiliser les attributs props dans les attribus de balise la syntaxe react est assez particuliere 

  render() {

    return (

      <div className='artist'>
        <div className='name'>
          {this.props.artist.name}: <span className='vote'>{this.props.artist.vote}</span>
        </div>
        
        <button className='plus' id={'plus-button-' + this.props.artist.name}  onClick={this.voteForThisArtist}>+</button>
        <button className='moins' id={'moins-button-' + this.props.artist.name}  onClick={this.unVoteForThisArtist}>-</button>
      
        <img src={'./' + this.props.artist.name + '.jpg'} className='photo' />
        <hr/>
      </div>
      

    );
  }

});

