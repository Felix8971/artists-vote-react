

// App component - represents the whole app
App = React.createClass({


  // This mixin makes the getMeteorData method work
  mixins: [ReactMeteorData],

  // Loads items from the Artistes collection and puts them on this.data.artistes
  getMeteorData() {
    return {
      artistes: Artistes.find({}, {sort: {vote: -1}}).fetch(),
      artistCount: Artistes.find({}).count(),
      currentUser: Meteor.user()
    }
  },


  // getArtists() {
  //   return [
  //     { _id: 1, name: "Francis Lalanne", vote:1 },
  //     { _id: 2, name: "Damien Saez", vote:5 },
  //     { _id: 3, name: "Jimi Hendrix", vote:3 }
  //   ];
  // },

  renderArtists() {
    // return this.getArtists().map((artist) => {
    //   return <Task key={artist._id} artist={artist} />;
    // });
    return this.data.artistes.map((artist) => {
      return <Task key={artist._id} artist={artist} />;
    });
  },

 //Ajout d'une nouvelle task
  handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    var name = ReactDOM.findDOMNode(this.refs.textInput).value.trim();

    Meteor.call("addArtist", name);//voir dans Meteor.methods({..

    // Clear form
    ReactDOM.findDOMNode(this.refs.textInput).value = "";
  },
  //pour le style dans les balises on utilise className au lieu de class

  render() {

    let nbrArtistTxt =  'Nombre d\'artistes';

    return (
      
      <div className="container">
        <header>
          <AccountsUIWrapper />
         
          <h1>Vote pour ton artiste préféré <span className='blue'>(React version)</span></h1>
          
          { this.data.currentUser ?
            <form className="new-artist" onSubmit={this.handleSubmit} >
              <h2 className='inline'>Ajouter un artiste </h2>
              <input
                type="text"
                ref="textInput"
                placeholder="Type to add new artist" />
            </form> : ''
          } 
          <h2> {nbrArtistTxt}:  
            <span className='blue'>{this.data.artistCount}</span>
          </h2>

          <h2 >Le meilleur est : 
            <span id='top-artist' className='blue'>?</span>
          </h2>
        </header>
        
        {this.renderArtists()}

      </div>

    );
  }

});