
// Define a collection to hold our artists

Artistes = new Mongo.Collection("artistes");


if (Meteor.isClient) {

  Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
  });

  Meteor.subscribe("artistes");

  // This code is executed on the client only
  Meteor.startup(function () {
    // Use Meteor.startup to render the component after the page is ready
    ReactDOM.render(<App />, document.getElementById("render-target"));
  });

  Deps.autorun(function () {

    setTimeout(function(){ //pas propre !!! 
	    var topArtist = Artistes.find({}, {sort: {vote: -1}, limit: 1}).fetch();
	    var name = topArtist[0].name;
	    console.log("top : " + name);
    	$('#top-artist').html(' ' + name + ' !'); 
    },2000);
  });

}



if (Meteor.isServer) {

  Meteor.startup(function () {//au lancement de meteor (commande meteor)
  	
  	console.log('Hello server side!');
	
	//Fixture
	if (Artistes.find().count() === 0) {
	  console.log('count === 0');	
	  Artistes.insert({
	    name: 'Damien Saez',
	    vote: 2
	  });

	  Artistes.insert({
	    name: 'Francis Lalanne',
	    vote: 5
	  });

	  Artistes.insert({
	    name: 'Jimi Hendrix',
	    vote: 3
	  });
	}

  });

  Meteor.publish("artistes", function () {

    return Artistes.find();

  });

}

Meteor.methods({

	voter(artistId, inc) {
		if (! Meteor.userId()) {
			throw new Meteor.Error("not-authorized");
		}
	  	var curVal = Artistes.findOne({_id:artistId},{vote:1}).vote;
	  	console.log("val:"+curVal);
	  	if (  inc > 0 || ( inc < 0 && curVal > 0) ){
	    	Artistes.update(artistId, {
	      	$set: {vote: curVal + inc }
	    	});
		}
	},


	addArtist(name) {
		// Make sure the user is logged in before inserting a task
		if (! Meteor.userId()) {
		  throw new Meteor.Error("not-authorized");
		}
		Artistes.insert({
		  name: name,
		  vote:0,
		  createdAt: new Date(),
		  owner: Meteor.userId(),
		  username: Meteor.user().username
		});
	},


    removeArtist(artistId){
    	if (! Meteor.userId()) {
		  throw new Meteor.Error("not-authorized");
		}
    	Artistes.remove(artistId);
    },

  // setChecked(taskId, setChecked) {
  //   Tasks.update(taskId, { $set: { checked: setChecked} });
  // }

});